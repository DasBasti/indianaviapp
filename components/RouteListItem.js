import React from "react";
import { Image, Text, View, StyleSheet, TouchableOpacity } from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";

export async function prepareRouteListItem(track, index, folder) {
  const sgmts = await track.loadAllSegmentInfo();

  let sumseg = sgmts[0];
  // sum all up skip first element
  for (let s in sgmts.slice(1)) {
    let seg = sgmts[s];
    sumseg.points.push(seg.points);
    sumseg.totalDistance += seg.totalDistance;
    sumseg.totalElevationGain += seg.totalElevationGain;
    sumseg.totalElevationLoss += seg.totalElevationLoss;
    // move bounding box bounds
    if (seg.boundingBox.lat_min < sumseg.boundingBox.lat_min) {
      sumseg.boundingBox.lat_min = seg.boundingBox.lat_min;
    }
    if (seg.boundingBox.lat_max > sumseg.boundingBox.lat_max) {
      sumseg.boundingBox.lat_max = seg.boundingBox.lat_max;
    }
    if (seg.boundingBox.lon_min < sumseg.boundingBox.lon_min) {
      sumseg.boundingBox.lon_min = seg.boundingBox.lon_min;
    }
    if (seg.boundingBox.lon_max > sumseg.boundingBox.lon_max) {
      sumseg.boundingBox.lon_max = seg.boundingBox.lon_max;
    }
  }
  let item = {
    id: index,
    folder: folder,
    name: track.getName(),
    infoText: "Total distance: " + sumseg.totalDistance + "m\n",
    //points: sumseg.points,
    boundingBox: sumseg.boundingBox,
    mapProgress: 0,
  };
  if (item.text === undefined) item.text = "Unknown";
  //console.log(item);
  return item;
}

export function RouteListItem({ item, selectItem, copyItem, deleteItem }) {
  let deleteIcon;
  if (deleteItem) {
    deleteIcon = (
      <TouchableOpacity
        onPress={() => {
          if (deleteItem) deleteItem(item.id);
        }}
      >
        <Icon
          style={styles.mapToolIcon}
          name="remove"
          size={20}
          color="firebrick"
        />
      </TouchableOpacity>
    );
  }
  let copyIcon;
  if (copyItem) {
    copyIcon = (
      <TouchableOpacity
        onPress={() => {
          if (copyItem) copyItem(item.id);
        }}
      >
        <Icon style={styles.mapToolIcon} name="copy" size={20} color="green" />
      </TouchableOpacity>
    );
  }

  return (
    <View style={styles.listItem}>
      <TouchableOpacity
        onPress={() => {
          if (selectItem) selectItem(item.id);
        }}
      >
        <View style={styles.listItemView}>
          <Image
            style={styles.mapIcon}
            source={require("../assets/nomap.png")}
          />
          <View sytle={styles.mapInfoView}>
            <Text style={styles.mapInfoText} numberOfLines={2}>
              {item.name}
            </Text>
            <Text style={styles.mapInfoDesc}>{item.infoText}</Text>
            <Text style={styles.mapInfoDesc}>
              Map progress: {item.mapProgress}%
            </Text>
          </View>
        </View>
      </TouchableOpacity>
      <View style={styles.mapTools}>
        {deleteIcon}
        {copyIcon}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  listItem: {
    backgroundColor: "#f8f8f8",
    borderBottomWidth: 1,
    borderColor: "#efefef",
    padding: 5,
  },
  listItemView: {
    flexDirection: "row",
  },
  mapTools: {
    flexDirection: "row",
  },
  mapToolIcon: {
    padding: 5,
  },
  mapIcon: {
    width: 80,
    height: 80,
    margin: 5,
  },
  mapInfoView: {
    //flexDirection: "row",
    flexWrap: "wrap",
  },
  mapInfoText: {
    fontSize: 16,
    //borderWidth: 1,
    //flexWrap: "wrap",
  },
  mapInfoDesc: {
    fontSize: 12,
    //borderWidth: 1,
  },
  deleteIcon: {
    alignContent: "flex-end",
  },
});
