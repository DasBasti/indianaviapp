import React, { useState } from "react";
import { Image, Text, View, StyleSheet, TouchableOpacity } from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";
import { openFile } from "../worker/TrackLoader";

function TopNavBar({ progress }) {
  const handlerAddTrack = () => {
    openFile();
  };
  const handlerSavedTracks = () => {
    alert("Saved tracks");
  };

  return (
    <View style={styles.container}>
      <Image source={require("../assets/title.png")} style={styles.logo} />
      <View style={styles.toolbar}>
        <TouchableOpacity
          style={styles.tollbarButton}
          onPress={() => handlerSavedTracks()}
        >
          <Icon name="save" size={40} color="gray" />
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.tollbarButton}
          onPress={() => handlerAddTrack()}
        >
          <Icon name="plus" size={40} color="gray" />
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#f8f8f8",
    borderBottomWidth: 1,
    borderColor: "#efefef",
    padding: 5,
  },
  logo: {
    resizeMode: "contain",
    width: "100%",
    height: 100,
    borderBottomWidth: 1,
    borderColor: "#efefef",
  },
  toolbar: {
    flexGrow: 1,
    flexDirection: "row",
  },
  tollbarButton: {
    padding: 5,
  },
});

export default TopNavBar;
