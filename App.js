import React, { useState } from "react";
import { SafeAreaView, StyleSheet, StatusBar, BackHandler } from "react-native";
import RouteLoaderScreen from "./screens/RouteLoaderScreen";
import SavedRoutesScreen from "./screens/SavedRoutesScreen";
import WelcomeScreen from "./screens/WelcomeScreen";

export default function App() {
  const [screen, setScreen] = useState({
    active: "welcome",
  });

  const changeScreen = (s) => {
    setScreen({ active: s });
  };

  BackHandler.addEventListener("hardwareBackPress", () => {
    if (screen.active === "welcome") {
      return false;
    }
    changeScreen("welcome");
    return true;
  });

  if (screen.active === "welcome") {
    return (
      <WelcomeScreen
        load={(s) => {
          changeScreen(s);
        }}
      />
    );
  }

  if (screen.active === "local") {
    return (
      <SafeAreaView style={styles.container}>
        <SavedRoutesScreen />
      </SafeAreaView>
    );
  }

  if (screen.active === "load") {
    return (
      <SafeAreaView style={styles.container}>
        <RouteLoaderScreen />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    paddingTop: StatusBar.currentHeight,
    margin: 5,
  },
});
