import * as DocumentPicker from "expo-document-picker";
import * as FileSystem from "expo-file-system";
import { GPXDocument } from "../react-native-gpx";
import uuid from "react-native-uuid";
import { prepareRouteListItem } from "../components/RouteListItem";

/**
 * Check if a folder exists and create it if it does not.
 * @param {String} folder_path
 */
const checkAndCreateFolder = async (folder_path) => {
  const folder_info = await FileSystem.getInfoAsync(folder_path);
  if (!Boolean(folder_info.exists)) {
    // Create folder
    try {
      await FileSystem.makeDirectoryAsync(folder_path, {
        intermediates: true,
      });
    } catch (error) {
      // Report folder creation error, include the folder existence before and now
      const new_folder_info = await FileSystem.getInfoAsync(folder_path);
      const debug = `checkAndCreateFolder: ${
        error.message
      } old:${JSON.stringify(folder_info)} new:${JSON.stringify(
        new_folder_info
      )}`;
      console.log(debug);
      Sentry.captureException(new Error(debug));
    }
  }
};

/**
 * Open a DocumentPicker to select GPX files from device storage and load the file.
 * @returns Array of RouteListItems
 */
export function openFile() {
  Promise.resolve(
    DocumentPicker.getDocumentAsync({ type: "application/gpx+xml" })
  ).then((res) => {
    if (res.type == "success") {
      return loadGPX(res.uri);
    }
  });
}

/**
 *
 * @param {String} xml of gpx file
 */
async function parseGPX(xml, folder) {
  let gpx = new GPXDocument(xml);
  // Track data
  let trks = await gpx.getTracks();
  trks = await Promise.all(
    trks.map(
      async (trk, index) => await prepareRouteListItem(trk, index, folder)
    )
  );
  return trks;
}

/**
 * Open and parse GPX file
 * @param {String} uri
 * @returns Array of RouteListItems
 */
export async function loadGPX(uri) {
  console.log("Reading:", uri);
  try {
    const xml = await FileSystem.readAsStringAsync(uri);
    let folder = uuid();
    // check if folder existes, if not create and copy the GPX into that folder
    await checkAndCreateFolder(FileSystem.documentDirectory + folder + "/")
      .then(
        FileSystem.copyAsync({
          from: uri,
          to: FileSystem.documentDirectory + folder + "/track.gpx",
        })
      )
      .then(console.log("Import to folder: " + folder));

    let trks = await parseGPX(xml, folder);
    console.log(trks);
    return trks;
  } catch (e) {
    console.error(e);
  }
}

/**
 * open a meta file and parse info
 * @param {String} uri
 */
async function openMetaFile(uri) {
  const meta = await FileSystem.readAsStringAsync(uri).then((text) => {
    return JSON.parse(text);
  });
  return meta;
}

/**
 * Parse track folder and load meta data if available
 *
 * @param {String} folder in documentDirectory
 * @returns Track meta information
 */
export async function openTrackFolder(folder) {
  console.log("Scan " + folder);
  return await FileSystem.getInfoAsync(
    FileSystem.documentDirectory + folder + "/meta.json"
  ).then(async (res) => {
    if (res.exists) {
      const meta = await openMetaFile(res.uri);
      console.log("meta file available.");
      return meta;
    } else {
      console.log("no meta file found. import needed.");
      const xml = await FileSystem.readAsStringAsync(
        FileSystem.documentDirectory + folder + "/track.gpx"
      );
      const trks = await parseGPX(xml, folder);
      return { track: trks[0] };
    }
  });
}
