import * as FileSystem from "expo-file-system";
import { TileHelper } from "../react-native-gpx";
import Constants from "../Constants";

/**
 * Download all the tiles needed for the track.
 *
 * Save the map tiles and parsed data to the folder
 *
 * @param {RouteListItem} track
 * @param {function} cb Status update function
 */
export default async function TileLoader(track, cb) {
  async function downloadSingleMapTileAsync(url, folder) {
    return await FileSystem.downloadAsync(
      Constants.mapServer + url,
      FileSystem.documentDirectory + folder + "/" + url.replace(/\//g, "_")
    ).then((res) => {
      return res.uri;
    });
  }

  async function fetchAllUrls(urls) {
    urls.forEach(async (url, u) => {
      await downloadSingleMapTileAsync(url.slug, track.folder)
        .then((uri) => {
          urls[u].uri = uri;
          //console.log("Download: " + url.slug + " to " + uri);
        })
        .then(() => {
          cb((100 * u) / (urls.length - 1), track.id);
        });
    });
    return urls;
  }

  let meta = { track: track };

  const tiles = {
    x: [
      TileHelper.lon2tile(track.boundingBox.lon_min, Constants.zoom),
      TileHelper.lon2tile(track.boundingBox.lon_max, Constants.zoom),
    ].sort(),
    y: [
      TileHelper.lat2tile(track.boundingBox.lat_min, Constants.zoom),
      TileHelper.lat2tile(track.boundingBox.lat_max, Constants.zoom),
    ].sort(),
    zoom: Constants.zoom,
  };
  meta.tiles = tiles;

  let urls = [];
  for (let x = tiles.x[0]; x <= tiles.x[1]; x++) {
    for (let y = tiles.y[0]; y <= tiles.y[1]; y++) {
      urls.push({
        slug: TileHelper.template(Constants.urlFormat, {
          x: x,
          y: y,
          z: Constants.zoom,
        }),
        uri: undefined,
      });
    }
  }

  // save urls
  fetchAllUrls(urls).then((urls) => {
    meta.urls = urls;
    // all loaded, save progress
    FileSystem.writeAsStringAsync(
      FileSystem.documentDirectory + meta.track.folder + "/meta.json",
      JSON.stringify(meta)
    ).then(() => {
      console.log(meta);
    });
  });
}
