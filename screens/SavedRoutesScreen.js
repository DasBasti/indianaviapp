import React, { useState, useEffect } from "react";
import {
  View,
  StyleSheet,
  Text,
  FlatList,
  ImagePropTypes,
  Alert,
} from "react-native";
import { RouteListItem } from "../components/RouteListItem";
import * as FileSystem from "expo-file-system";
import TopNavBar from "../components/TopNavBar";
import { openTrackFolder } from "../worker/TrackLoader";

function SavedRoutesScreen(props) {
  const [state, setState] = useState({
    gpxTracks: [],
  });

  const handleCopyItem = (id) => {
    alert("copy " + id);
  };

  const handleDeleteItem = (id) => {
    Alert.alert("Delete Track?", "Permanently delete track " + id, [
      { text: "No" },
      { text: "Yes", onPress: () => handleDeleteTrack(id) },
    ]);
  };

  const handlerSelectItem = (id) => {
    alert("selected " + id);
  };

  const handleDeleteTrack = (id) => {
    Promise.resolve(
      FileSystem.deleteAsync(
        FileSystem.documentDirectory + state.gpxTracks[id].folder
      )
    ).then();
    state.gpxTracks.splice(id, 1);
    clearAndReloadList();
  };

  const loadAvailableTracks = async () => {
    const folders = await FileSystem.readDirectoryAsync(
      FileSystem.documentDirectory
    );
    let listIndex = 0;
    for (let i in folders) {
      const folder = await FileSystem.getInfoAsync(
        FileSystem.documentDirectory + folders[i]
      );
      if (folder.isDirectory) {
        openTrackFolder(folders[i]).then((meta) => {
          //console.log(meta);
          state.gpxTracks.push(meta.track);
          listIndex = listIndex + 1;
          setState({ gpxTracks: state.gpxTracks });
        });
      }
    }
  };

  // run one at loading the component
  useEffect(() => {
    clearAndReloadList();
  }, []);

  function clearAndReloadList() {
    state.gpxTracks = [];
    Promise.resolve(loadAvailableTracks()).then(
      setState({ gpxTracks: state.gpxTracks })
    );
  }

  return (
    <View>
      <TopNavBar />
      <FlatList
        data={state.gpxTracks}
        renderItem={({ item }) => (
          <RouteListItem
            item={item}
            copyItem={handleCopyItem}
            deleteItem={handleDeleteItem}
            selectItem={handlerSelectItem}
          />
        )}
        keyExtractor={(item, index) => index.toString()}
      />
    </View>
  );
}

export default SavedRoutesScreen;

const styles = StyleSheet.create({});
