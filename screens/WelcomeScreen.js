import React from "react";
import {
  StyleSheet,
  ImageBackground,
  View,
  Image,
  Text,
  TouchableNativeFeedback,
} from "react-native";
import Constants from "../Constants";

function WelcomeScreen(props) {
  return (
    <ImageBackground
      style={styles.background}
      source={require("../assets/background.png")}
      blurRadius={2}
    >
      <View style={styles.logoContainer}>
        <Image source={require("../assets/title.png")} style={styles.logo} />
        <Text>Und die letzte Kreuzung</Text>
      </View>
      <TouchableNativeFeedback
        onPress={() => {
          props.load("local");
        }}
      >
        <View style={styles.localRoutesButton}>
          <Text>Saved Routes</Text>
        </View>
      </TouchableNativeFeedback>
      <TouchableNativeFeedback
        onPress={() => {
          props.load("load");
        }}
      >
        <View style={styles.loadRoutesButton}>
          <Text>Load Routes</Text>
        </View>
      </TouchableNativeFeedback>
    </ImageBackground>
  );
}

const styles = StyleSheet.create({
  background: {
    flex: 1,
    justifyContent: "flex-end",
    alignItems: "center",
    backgroundColor: "#fff",
  },
  logo: {
    resizeMode: "contain",
    width: "100%",
    height: 100,
  },
  logoContainer: {
    position: "absolute",
    width: "90%",
    top: 50,
    alignItems: "center",
  },
  localRoutesButton: {
    width: "100%",
    height: 70,
    backgroundColor: Constants.color_red,
    alignItems: "center",
    justifyContent: "center",
  },
  loadRoutesButton: {
    width: "100%",
    height: 70,
    backgroundColor: Constants.color_green,
    alignItems: "center",
    justifyContent: "center",
  },
});

export default WelcomeScreen;
