import React, { useState } from "react";
import { Text, View, StyleSheet, Button, FlatList, Alert } from "react-native";
import * as FileSystem from "expo-file-system";
import { RouteListItem } from "../components/RouteListItem";
import TileLoader from "../worker/TileLoader";
import { openFile, loadGPX } from "../worker/TrackLoader";

import TopNavBar from "../components/TopNavBar";

function RouteLoaderScreen(props) {
  const [state, setState] = useState({
    downloadProgress: 0,
    mapProgress: 0,
    gpxTracks: [],
    modalView: undefined,
  });
  let folder;

  async function downloadTestFile() {
    const callback = (downloadProgress) => {
      const progress =
        downloadProgress.totalBytesWritten /
        downloadProgress.totalBytesExpectedToWrite;
      setState({ ...state, downloadProgress: Math.floor(progress * 100) });
    };

    const downloadResumable = FileSystem.createDownloadResumable(
      "https://datentonne.wlankaffee.de/s/qFZod7jE7s26EGS/download",
      FileSystem.documentDirectory + "test.gpx",
      {},
      callback
    );

    try {
      const { uri } = await downloadResumable.downloadAsync();
      console.log("Finished downloading to ", uri);
      let trks = await loadGPX(uri);
      if (trks.length) setState({ ...state, gpxTracks: trks });
    } catch (e) {
      console.error(e);
    }
  }

  const tracks = state.gpxTracks;

  const handleDownloadCallback = (progress, id) => {
    const tracks = state.gpxTracks;
    tracks[id].mapProgress = progress;
    setState({ ...state, gpxTracks: tracks });
  };

  const handlerPrepareMap = (track) => {
    TileLoader(track, handleDownloadCallback);
  };

  const handlerSelectItem = (id) => {
    Alert.alert(
      "Prepare Map for Device",
      "Should this map be prepared for the device?",
      [
        { text: "No" },
        { text: "Yes", onPress: () => handlerPrepareMap(tracks[id]) },
      ]
    );
  };
  const downloadProgress = state.downloadProgress;
  const showList = () => {
    return (
      <View style={styles.container}>
        <TopNavBar />
        <Text style={styles.heading}>Route Loader</Text>
        <Button
          style={styles.button}
          title="load GPX file"
          onPress={() => {
            let trks = openFile();
            if (trks.length) setState({ ...state, gpxTracks: trks });
          }}
        />
        <View style={{ height: 10 }}></View>
        <Button
          style={styles.button}
          title="Download sample track"
          onPress={downloadTestFile}
        />
        <Text>Test Route Download: {downloadProgress}%</Text>
        <View style={{ height: 10 }}></View>
        <Text>GPX File contains</Text>
        <View style={styles.listView}>
          <FlatList
            style={styles.list}
            data={tracks}
            renderItem={({ item }) => (
              <View>
                <RouteListItem item={item} selectItem={handlerSelectItem} />
              </View>
            )}
            keyExtractor={(item, index) => index.toString()}
          />
        </View>
      </View>
    );
  };

  return showList();
}

const styles = StyleSheet.create({
  container: {
    margin: 5,
  },
  button: {
    marginVertical: 5,
  },
  listView: {},
  heading: {
    fontSize: 16,
    alignSelf: "center",
  },
});

export default RouteLoaderScreen;
