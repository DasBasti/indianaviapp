export default Constants = {
  zoom: 15,
  mapServer: "https://tile.openstreetmap.org/",
  urlFormat: "{z}/{x}/{y}.png",
  color_red: "#800000",
  color_green: "#008000",
};
