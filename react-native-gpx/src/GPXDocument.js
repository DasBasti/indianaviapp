var XMLParser = require("react-xml-parser");

import GPXTrack from "./GPXTrack.js";

export default class GPXDocument {
  /**
   * Parse the gpx xml string and store the results internally
   * @param {string} xmlString The xml string to parse
   */
  constructor(xmlString) {
    this.parsedGPX = new XMLParser().parseFromString(xmlString);
  }

  /**
   * Get the tracks for all tracks in the gpx file
   */
  async getTracks() {
    let tracks = this.parsedGPX.getElementsByTagName("trk");

    if (tracks === undefined) throw new Error("Unable read tracks");

    // Use the map function to get an array with the name of each track if it exists
    return tracks.map((val) => {
      return val === undefined ? undefined : new GPXTrack(val);
    });
  }

  /**
   * Get Document meta data
   */
  async getMetaData() {
    let meta = this.parsedGPX.getElementsByTagName("metadata")[0];

    if (meta === undefined) throw new Error("Unable to read metadata");

    let ret = {};
    try {
      ret.name = meta.getElementsByTagName("name")[0].value;
    } catch (e) {
      ret.name = e.toString();
    }

    try {
      ret.creator = meta
        .getElementsByTagName("author")[0]
        .getElementsByTagName("link")[0]
        .getElementsByTagName("text")[0].value;
    } catch (e) {
      ret.creator = e.toString();
    }

    try {
      ret.link = meta
        .getElementsByTagName("author")[0]
        .getElementsByTagName("link")[0].attributes.href;
    } catch (e) {
      ret.link = e.toString();
    }

    return ret;
  }
}

// The mean radius of the earth in miles
GPXDocument.MEAN_RAD_MI = 3958.7613;
GPXDocument.MEAN_RAD_KM = 6371;
GPXDocument.GPX_NS = { ns: "http://www.topografix.com/GPX/1/1" };
